import React from "react";
import CustomButton from "./CustomButton";

const AIPicker = ({
  prompt,
  setPrompt,
  generatingImg,
  handleSubmitGenerateImg,
}) => {
  return (
    <div className="aipicker-container">
      <textarea
        className="aipicker-textarea"
        placeholder="Enter your prompt here"
        rows={5}
        value={prompt}
        onChange={(e) => setPrompt(e.target.value)}
      />
      <div className="flex flex-wrap gap-3">
        {generatingImg ? (
          <CustomButton
            type="outline"
            title="Asking AI ..."
            customStyle="text-xs"
          />
        ) : (
          <>
            <CustomButton
              type="outline"
              title="AI logo"
              handleClick={() => handleSubmitGenerateImg('logo')}
              customStyle="text-xs"
            />
            <CustomButton
              type="filled"
              title="AI Full"
              handleClick={() => handleSubmitGenerateImg('full')}
              customStyle="text-xs"
            />
          </>
        )}
      </div>
    </div>
  );
};

export default AIPicker;
